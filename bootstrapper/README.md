# Bootstrap cluster hosts

* `os_install/` contains documentation about OS installation for remote nodes
* The rest is Ansible configuration 

## Configuration goals

* Configure access rights
    
    User creation, upload their ssh keys, set passwordless sudo...

* Installation of base toolset through package manager 

* Installation of complex tooling (e.g. Docker, Nomad, Vault, Consul) through other means (custom APT repo?)

* Basic configuration of the server 

    * Docker log rotation 

## New stuff to install on Kimsufi

* A Docker registry!