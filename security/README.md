## PHP inections

See phpmalwarescanner


## Firewall

### Ports I need

Incoming - DROP except:
* http/s (nginx)
* 8448 (synapse - with TLS through nginx)
* ssh
* icmp
* ftp

Outgoing - ACCEPT all

### Using UFW

* Enable IPv6 in `/etc/default/ufw` if not done:

    IPV6=yes

* Set default rules e.g.:

    ufw default deny incoming
    ufw default allow outgoing

* Configure more rules:

    ufw [allow|deny|reject|limit] [in|out] [protocol|port]

See `ufw.sh`.
