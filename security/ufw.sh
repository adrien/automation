#!/bin/bash


ufw default allow outgoing
ufw default allow routed
ufw default deny incoming
ufw allow in ftp/tcp
ufw allow in ssh/tcp
ufw allow in http/tcp
ufw allow in https/tcp
ufw allow in 8448
ufw allow in from 172.0.0.0/8 # docker and such

