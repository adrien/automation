---

encrypted_drive:
    command: "/sbin/mount_vault"
    password: "{{ vault_encrypted_drive_password }}"

mysql_root_password: "{{ vault_mysql_root_password }}"
adrien_serenity_password: "{{ vault_adrien_serenity_password }}"

www_path: /vault/www
sites_path: /vault/sites
backups_path: /vault/backups

wordpress:
  version: 5.4
  checksum: sha1:d5f1e6d7cadd72c11d086a2e1ede0a72f23d993e
gitea:
  version: 1.15.4
synapse:
  version: v1.48.0
drupal: 
  version: 8.9.7-apache
nextcloud:
  version: 19.0.3
  checksum: md5:2094204fd0c3471be2ec010a71231da6

postgres:
  pg_hba_path: "/etc/postgresql/9.6/main/pg_hba.conf"

sites:
  - slug: rdb # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: rennesdesbois.fr
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: yes
    # What kind of site is that?
    type: wordpress
    # Subnet addresses
    subnet_cidr_address: 172.27.1.0/24
    subnet_gateway_ip: 172.27.1.1
    subnet_nginx_ip: 172.27.1.2
    subnet_site_ip: 172.27.1.3
    # MySQL
    mysql_database: rdb
    mysql_username: rdb
    mysql_password: "{{ vault_rdb_mysql_password }}"

  - slug: arvuhez # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: arvuhez.org
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: wordpress
    # Subnet addresses
    subnet_cidr_address: 172.27.2.0/24
    subnet_gateway_ip: 172.27.2.1
    subnet_nginx_ip: 172.27.2.2
    subnet_site_ip: 172.27.2.3
    # MySQL    
    mysql_database: arvuhez
    mysql_username: arvuhez
    mysql_password: "{{ vault_arvuhez_mysql_password }}"

  - slug: zinzoscope # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: zinz.luxeylab.net
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: wordpress
    # Subnet addresses
    subnet_cidr_address: 172.27.3.0/24
    subnet_gateway_ip: 172.27.3.1
    subnet_nginx_ip: 172.27.3.2
    subnet_site_ip: 172.27.3.3
    # MySQL
    mysql_database: zinzoscope
    mysql_username: zinzoscope
    mysql_password: "{{ vault_zinzoscope_mysql_password }}"

  - slug: lexperimental # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: lexperimental.fr
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: wordpress
    # Subnet addresses
    subnet_cidr_address: 172.27.4.0/24
    subnet_gateway_ip: 172.27.4.1
    subnet_nginx_ip: 172.27.4.2
    subnet_site_ip: 172.27.4.3
    # MySQL
    mysql_database: lexperimental
    mysql_username: lexperimental
    mysql_password: "{{ vault_lexperimental_mysql_password }}"

  - slug: mts # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: editionsmangetasoupe.fr
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: drupal
    # Subnet addresses
    subnet_cidr_address: 172.27.5.0/24
    subnet_gateway_ip: 172.27.5.1
    subnet_site_ip: 172.27.5.2
    # This will allow setting up MySQL
    # Configuration on Drupal's side must be done by hand:
    # Edit your <drupal_install>/sites/default/settings.php
    mysql_database: mts8
    mysql_username: mts
    mysql_password: "{{ vault_mts_mysql_password }}"

  - slug: gitea # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: git.deuxfleurs.fr
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: gitea
    # Subnet addresses
    subnet_cidr_address: 172.27.6.0/24
    subnet_gateway_ip: 172.27.6.1
    subnet_site_ip: 172.27.6.2
    # User IDs
    user_name: git
    user_uid: 1007
    user_group: git
    user_gid: 1006
    # MySQL
    mysql_database: gitea
    mysql_username: gitea
    mysql_password: "{{ vault_gitea_mysql_password }}"

  - slug: synapse # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: zinz.dev
    # Wanna display custom HTML as landing page? Fill this with its path.
    # To keep the defaults, comment line
    custom_landing: /vault/www/riot
    # What kind of site is that?
    type: synapse
    # User IDs
    user_uid: 33 # www-data
    user_gid: 33 # www-data
    # Subnet addresses
    subnet_cidr_address: 172.27.7.0/24
    subnet_gateway_ip: 172.27.7.1
    subnet_site_ip: 172.27.7.2
    # PostgreSQL
    #postgres_host: db
    postgres_database: synapse
    postgres_username: synapse
    postgres_password: "{{ vault_synapse_postgres_password }}"
    # SMTP
    contact_email: contact@zinz.dev 
    smtp_host: mail.gandi.net
    smtp_port: 587 # Clear: 25, SSL: 465, STARTTLS: 587
    smtp_username: contact@zinz.dev 
    smtp_password: "{{ vault_smtp_contact_at_zinz_dev_password }}"
    # Secrets
    registration_shared_secret: "{{ vault_synapse_registration_shared_secret }}"
    # Others
    max_upload_size: 20M
    # Coturn server
    coturn:
      # URL is fixed to "turn.{{ site.url }}" - take that for granted
      static_auth_secret : "{{ vault_synapse_coturn_static_auth_secret }}"
      listening_port: 3578
      min_port: 49152
      max_port: 49172
      external_ip: 92.243.8.85
      # 4 streams per video call, so 12 streams = 3 simultaneous relayed calls per user.
      # Set to null if you want to disable quota
      user_quota: 12
      total_quota: 1200
      denied_peer_ips: []
        #- '10.0.0.0-10.255.255.255'
        #- '172.16.0.0-172.31.255.255'
      allowed_peer_ips: ['10.0.0.1']


  - slug: cloud # Shorthand name to use as directory/file name
    # The site URL (without www)
    url: cloud.luxeylab.net
    # Ask nginx to redirect url to www
    # Else, we redirect www to url
    redirect_to_www: no
    # What kind of site is that?
    type: nextcloud
    # Subnet addresses
    subnet_cidr_address: 172.27.8.0/24
    subnet_gateway_ip: 172.27.8.1
    subnet_nginx_ip: 172.27.8.2
    subnet_site_ip: 172.27.8.3
    # MySQL
    mysql_database: cloud
    mysql_username: cloud
    mysql_password: "{{ vault_cloud_mysql_password }}"
