# Automation: because 10h of coding can save 10m of your life!

Go to [deployer](deployer/), where the fun is at.

## Structure

* [`/deployer`](deployer/) contains deployment Ansible code for Serenity. **Being outdated** by migration to new server (HammerHead).
* [`/bootstrapper`](bootstrapper/) contains Ansible configuration code for new servers (notably HammerHead). Will only take interest in the bootstrapping phase of the host. Service deployment will be handled by Nomad et al.

## TODO

* Host bootstrap Ansible code to [`/bootstrapper`](bootstrapper/)

# Random notes 

## How to package Wordpress

### Wordpress + PHP-FPM in Docker + nginx 

* [Dockerise your PHP app with PHP-FPM and nginx](http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/)

nginx and PHP-FPM both need access to the files--at the same location. It's thus not easy to have a single nginx serving multiple PHP-FPM containers. You always need a webserver in the same container as PHP-FPM. 

### Alternatives

* [WP multisite + Caddy](https://skippy.net/caddy-docker-php-wordpress): arguments against using the Wordpress Docker image, keeps the whole wp install outside the image, and instead focuses on properly configuring PHP-FPM. Has the advantage of being reusable for other PHP projects. Is well argumented. Tackles file access rights and mailing (ssmtp), it's a good source!

#### Which webserver?

traefik does not support php-fpm, Caddy does. Not such a problem, we can keep Apache inside the container, who gives a shit.

### Wordpress security

* [File permissions](https://wordpress.org/support/article/changing-file-permissions/)
* [Sécu Wordpress (fr)](http://wptheme.fr/guide-creation-site-blog/securiser-et-nettoyer-theme-wordpress-virus/)




